  +++
title = "Fedora 34 Setup Guide"
description = "A friendly setup guide for asusctl"
sort_by = "none"
template = "page/wiki.html"
author = "Jani Kahrama"
+++

# Fedora 34 and asusctl Setup

The recommended flavour of Linux by [asus-linux.org](http://asus-linux.org) is Fedora, for its ease of use for new users, friendly community, and good support for the latest hardware. This guide is written for Fedora 34.

This guide does not cover the choices of running Windows and Linux, or only Linux on your device, and their respective partitioning requirements.

For additional information, see the installation instructions:
[Official Fedora install guide](https://docs.fedoraproject.org/en-US/fedora/f34/install-guide/)

For simple USB stick flashing:
[Fedora Media Writer](https://getfedora.org/en/workstation/download/)

# Preparations

### Backup Propietary eSupport Drivers Folder

Stock installations of Windows on ASUS laptops include propietary drivers that cannot be sourced directly from the ASUS website or the MyASUS utility. Before removing the Windows partition or recovery partition these drivers should be backed up. If you ever decide to dual boot or run Windows in a VM, you will need a copy of the drivers for your specific model.

The folders can be found in `C:\eSupport`

Make sure to backup this folder before performing any destructive operations on your Windows partition!

### Disable Secure Boot
To make sure Nvidia drivers and the necessary support modules work without issues, Secure Boot must be disabled in the UEFI.

1. Press DEL repeatedly during boot to enter UEFI setup screen
2. Press F7 for advanced mode
3. Security → Secure Boot Control → Disable
4. Save and exit

### Use the Laptop Screen
Due to display signal routing on Asus ROG laptops, and the setup process dealing with multiple graphics devices, having external screens connected during setup may result in unpredictable behavior. Please follow this guide with all external displays disconnected.

---

# Installation

1. Download Fedora 34 Workstation ISO file from [WORK iso](https://dl.fedoraproject.org/pub/alt/live-respins/) and write it to a USB stick. You *must* use one of these ISO instead of the official if you have 2021+ hardware.
2. On booting from USB, in the Fedora boot menu, select:
Troubleshooting → Start Fedora in basic graphics mode
3. Follow the steps of the installer, and remove the USB stick when you reboot

---

# Setup

### Update the System

System update should be done after setting up the repos and copr.

#### Using the Terminal
This guide requires typing *terminal commands.* To type them, start the Terminal application, which opens a window that has a command prompt.

To open the Terminal, simply press the Windows/Meta key to bring up the Activities view, and start typing "term" in the search box. Click on the search result.

![Terminal in search bar](/images/guide_terminal.png)

Commands that have *sudo* in front are administrator commands, and may require you to type in your password.


#### Edit Default Package Repositories

First, edit the package repositories to exclude kernel updates from Fedora; asusctl uses a copr to get updates:

1. Edit the package repos:
    ```
    sudo gedit /etc/yum.repos.d/fedora.repo /etc/yum.repos.d/fedora-updates.repo /etc/yum.repos.d/fedora-updates-testing.repo
    ```
    NOTE: The above command opens *three* files, each in their own tab. All three files must be edited according to the instructions below.

    At the end of the block of lines starting with `[fedora]`, `[updates]`, and `[updates-testing]` respectively in each file add this line at the end of the block:
    ```
    exclude=kernel kernel-core kernel-devel
    ```
    The end result will look similar to:
    ```
    [updates-testing]
    name=Fedora $releasever - $basearch - Test Updates
    #baseurl=http://download.example/pub/fedora/linux/updates/testing/$releasever/Everything/$basearch/
    metalink=https://mirrors.fedoraproject.org/metalink?repo=updates-testing-f$releasever&arch=$basearch
    enabled=0
    countme=1
    repo_gpgcheck=0
    type=rpm
    gpgcheck=1
    metadata_expire=6h
    gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
    skip_if_unavailable=False
    exclude=kernel kernel-core kernel-devel

    [updates-testing-debuginfo]
    ```
    Note the new-line after the `exclude=` line.

    Save the files and close the editor.

2. Add the copr for asusctl and rog kernel
    ```
    sudo dnf copr enable lukenukem/asus-linux
    ```

#### Do Update

1. Open Software store
2. Navigate to Updates tab
3. Click the Refresh-button in the top left corner
4. Download all available updates

    ![Software Updates](/images/guide_system_download.png)

5. After the updates have been downloaded, click the "Restart & Update" button

    ![Software Updates](/images/guide_system_restart.png)

6. When the updates have been installed, reboot your computer

NOTE: Upon restart the device is still possibly running in software rendering mode, symptoms:
- Laggy desktop
- Black screen after reboot
- Suspend on lid close not working
- Screen brightness control not working
- Multi-touch gestures not working
This will all be addressed in the later steps.

### Install Nvidia Graphics Drivers

The next step is to install the drivers for the Nvidia GPU.

1. Open Software store
2. Enable 3rd party repositories
3. Click on the hamburger menu, select Software Repositories

    ![Software repositories menu](/images/guide_repositories.png)

4. Enable RPM Fusion for Fedora 34 - Nonfree - NVIDIA Driver

    ![Nvidia repository](/images/guide_nvidia.png)

5. Input the following terminal commands:

    ```bash
    sudo dnf update -y
    sudo dnf install kernel-devel
    sudo dnf install akmod-nvidia
    sudo dnf install xorg-x11-drv-nvidia-cuda
    sudo dnf install xorg-x11-drv-nvidia-power
    ```

6. Enable Nvidia power services:

    ```bash
    sudo systemctl enable nvidia-hibernate.service nvidia-suspend.service nvidia-resume.service
    ```


### Update System Boot Configuration
1. In Terminal, launch the text editor to edit the system boot options.

    ```bash
    sudo gedit /etc/default/grub
    ```

    Edit the file to look like below.

    ```
    GRUB_TIMEOUT=5
    GRUB_DISTRIBUTOR="$(sed 's, release .*$,,g' /etc/system-release)"
    GRUB_DEFAULT=saved
    GRUB_DISABLE_SUBMENU=true
    GRUB_TERMINAL_OUTPUT="console"
    GRUB_CMDLINE_LINUX="rd.driver.blacklist=nouveau modprobe.blacklist=nouveau nvidia-drm.modeset=0 rhgb quiet"
    GRUB_DISABLE_RECOVERY="true"
    GRUB_ENABLE_BLSCFG=true
    ```

    There are usually only two modifications to make:

    1. Change the value of `nvidia-drm.modeset=1` to `nvidia-drm.modeset=0`. This change is needed to allow switching between graphics devices without needing a reboot.
    2. Fedora installer may create duplicates of `rd.driver.blacklist=nouveau`, `modprobe.blacklist=nouveau`, and `nvidia-drm.modeset=1`. These duplicates can be safely deleted.

    Optionally, if the install guide has been deviated from, the same line of text that contains the previous changes may also contain a standalone word `nomodeset`. This must be removed or graphics acceleration is disabled.

    Save the file and close the editor.

2. Update grub:

    The edits made to the grub file need to be updated to the system. This is just a single terminal command.

    ```bash
    sudo grub2-mkconfig -o /etc/grub2.cfg
    ```

3. Reboot


### Install asusctl
The final section is to install asusctl and its supporting software. This enables controls for the Asus ROG hardware on the laptop.

1. Install asusctl

    ```bash
    sudo dnf install asusctl power-profiles-daemon
    sudo dnf update --refresh
    sudo systemctl enable supergfxd.service power-profiles-daemon.service
    ```

2. Reboot

    After the reboot, you should have a fully functional Fedora laptop, with ROG hardware features enabled.

Please note that asusctl is under continuous development, and will improve in capability over time.

---

# Optional Steps

### Keyboard Backlight
If the keyboard backlight does not work automatically, set a mode in asusctl:

```bash
asusctl led-mode static
```

### RGB Backlights
If an RGB capable laptop has no led modes available, then the /etc/asusd/asusd-ledmodes.toml file needs adjusting:

```bash
cat /sys/class/dmi/id/product_family # provides the prod_family var,
cat /sys/class/dmi/id/board_name # provides name to be added to board_names.
```
Edit the configuration file and add the needed values:
```bash
sudo gedit /etc/asusd/asusd-ledmodes.toml
```
Save the file and close the text editor.

### Switching from Nvidia GPU to AMD Integrated
If the laptop has booted in Nvidia mode, switching to AMD integrated graphics and the Wayland desktop requires a reboot. To fix this so the switch only requires a logout/login, change the following:

```bash
sudo gedit /lib/udev/rules.d/61-gdm.rules
```
Edit this line of text to have a hash in the front, as in the example. This will disable the setting.
```bash
# DRIVER=="nvidia", RUN+="/usr/libexec/gdm-disable-wayland"
```

You ma
Save the file and close the editor.

Reboot to enable the change.

### Hide an Unnecessary Boot Message
When booting the computer in integrated mode, a message displays "Nvidia kernel module not found. Falling back to Nouveau." This is harmless and normal for asusctl integrated mode, but may feel distracting. To disable the message, input this terminal command:

```bash
sudo systemctl mask nvidia-fallback.service
```

### Desktop Wigdets
A desktop widget to operate asusctl is currently in development:
[asusctl Gnome extension](https://gitlab.com/asus-linux/asusctl-gex)


# Known Limitations

### Nvidia GPU suspend issue on 2020 models of Zephyrus G14 and G15
Be aware that the Nvidia GPU on the 2020 models of G14 and G15 has an issue concerning power draw. In general, the GPU fails to enter a low-power state even when idle, and this causes unnecessary power draw and heat generation. This issue has been addressed in the 2021 models. Things have improved for the 2020 models with Wayland and the 470 driver (read below).

### Nvidia Graphics on Wayland Desktop
The default Gnome desktop in Fedora 34 uses a new protocol called Wayland. Since the 470 Nvidia driver version it is possible to run Wayland desktop on Nvidia GPUs. This is a major update to the capability and utility of Nvidia graphics on ROG laptops, but because this is a new development, there are still lingering issues.

1. Exclusive Nvidia mode does not work
    Currently the asusctl Nvidia mode which runs everything exclusively on the more powerful GPU does not work. This is due to a remaining issue on the 470 driver related to technology called Reverse-PRIME, a method of passing graphics rendered by the Nvidia GPU back to the AMD integrated graphics.

2. USB-C DisplayPort output does not work on Wayland
    Another Reverse-PRIME issue is that if you need to use an external display with the DisplayPort connection (via USB-C or a dock), it only works on the X.org desktop. This issue does not affect all ROG laptop models, but depends on their internal signal routing.

3. Compute vs. hybrid on Zephyrus G14 and G15
    Currently the best way to take advantage of Nvidia graphics on Wayland desktop is to use asusctl integrated graphics mode, and enable compute if you wish to run applications using the Nvidia GPU. In compute mode, the Nvidia GPU automatically suspends even on 2020 models when it has no active processes, reducing power consumption. On these laptops, compute mode can also be disabled manually without requiring a logout/login.

    Hybrid mode is also operational on Wayland, and there are early reports of a slight performance benefit. On 2020 G14 and G15 the hybrid mode suffers from the GPU suspend issue, increasing power consumption and heat output when idle.

    Both modes currently offer the same functionality despite their naming because Wayland does not respect X.org settings.

4. Disabling compute
    If asusctl fails to disable compute, there are likely active processes still using the Nvidia device. Check for these with `nvidia-smi` and `lsof /dev/nvidia*` commands.

5. Resuming from suspend
    Applications that were running on the Nvidia GPU may have all-black windows after resuming from sleep. No workaround for this issue exists yet. Enabling Nvidia suspend/resume/hibernate services does not address the matter.

For older Nvidia drivers (pre-470) whenever Nvidia graphics are enabled, in either standalone or hybrid mode, using the X.org desktop is necessary.

To select the correct desktop environment, click on the gear icon in the bottom right corner of the login screen. Select "Gnome" with asusctl in integrated graphics mode to run Wayland, or "Gnome on Xorg" when Nvidia graphics are enabled in hybrid or exclusive mode.
